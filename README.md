This Project has been completed By Iman Kazemini Moghaddam and its part of the hiring procedure for a company in Germany. 

 Technologies are included:
 
 1- MVVM as an architecture
 
 2- RXSwift
 
 3- CleanCode
 
 4- TDD as one of the requirements of the project (Test Driven Development)

 
 Reasons:
 1- MVVM is a pioneering architecture which brings great abilities such as readability, testability, and in most of the situation less time for developing. 
 2- RXSwift has been used to reduce the lines of code and support the MVVM better.
 3- Cleancode is an essential part of any project, there is no dependency between the size of the project or other indicators and using CleanCode. The value of the principles
 in CleanCode have a lot of advantages. For more information: https://de.wikipedia.org/wiki/Clean_Code
 
 
 
 
 Notes:
 Base on the requirements, I didn't have a specific assortment or concent about the UI elements. Also, I didn't have information or requirement about the SDK version which 
 must be supported for users. So I assumed my UI/UX and designing and Target SDK are not targeted.
 
 
 
 