//
//  MainViewModel.swift
//  Hexad ChallangeCode
//
//  Created by Iman Kazemini on 1/14/20.
//  Copyright © 2020 HexadChallange. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class MainViewModel {
    
    public var favoritesArray: Variable<[FavoriteModel]> = Variable(JosnFavorites.getFavorites().parse(to: [FavoriteModel].self)!)
    public var isInRandomTime:Variable<Bool> = Variable(false)
    
    var disposeBag = DisposeBag()
    var timerDisposable:Disposable!
    
    init() {
        setSort()
        setRandomTimerObsever()
    }
    
    public func  setSort() {
        favoritesArray.value = favoritesArray.value.sorted(by: { $0.rate > $1.rate })
    }

    public func setRandom(){
        for (index,_) in favoritesArray.value.enumerated() {
            favoritesArray.value[index].rate = Double.random(in: 1..<11)
        }
    }
    
    //this method trigger from outside to change the rate by user
    func setNewRate(index:Int,newRate:Double){
        favoritesArray.value[index].rate = newRate
    }
    
    //Monitoring the flag to start a randomized timer to change the rates random
    func setRandomTimerObsever(){
        isInRandomTime.asObservable()
        .subscribe({ value in
            if value.element! {
                self.startRandomTime()
            }
        }).disposed(by: disposeBag)
    }
    
    //Trigger a scheduler(interval) to run random
     func startRandomTime(){
        if isInRandomTime.value {
            timerDisposable =  Observable<Int>.interval(Double.random(in: 1..<5), scheduler: MainScheduler.instance)
                .subscribe({ _ in
                    if self.isInRandomTime.value {
                       self.setRandom()
                       self.setSort()
                    }else{
                        self.timerDisposable.dispose()
                    }
            })
            timerDisposable.disposed(by: disposeBag)
        }
    }
    
    deinit {
        print("MainViewModel deinited correctly")
    }
    
}
