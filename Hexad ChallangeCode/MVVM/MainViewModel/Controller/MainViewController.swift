//
//  ViewController.swift
//  Hexad ChallangeCode
//
//  Created by Iman Kazemini on 1/14/20.
//  Copyright © 2020 HexadChallange. All rights reserved.
//

import UIKit
import Foundation
import RxSwift

class MainViewController: UIViewController {
   
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var randomButtom: UIBarButtonItem!
    
    let TableCellId = "TableCellId"
    
    var mainViewModel = MainViewModel()
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTableRows()
        setRandomButton()
            
    }
    
    //initializing Tableview and setting its data from ViewModel
    private func setTableRows(){
            self.mainViewModel.favoritesArray
            .asObservable()
            .bind(to: self.tableView.rx.items(cellIdentifier: "TableCellId", cellType: FavoriteTableViewCell.self)) { (index, model, cell) in
                cell.name.text = model.name
                cell.rate.rating = model.rate
                cell.rate.didFinishTouchingCosmos = { newRate in
                    self.mainViewModel.setNewRate(index: index, newRate: newRate)
                    self.mainViewModel.setSort()
                }
            }
            .disposed(by: disposeBag)
    }
    
    
    // setting observable on the random button and simultaneously setting its observer for monitoring state to change the label of the button
    private func setRandomButton(){
        
        mainViewModel.isInRandomTime
            .asObservable()
            .subscribe({ value in
                value.element! ? (self.randomButtom.title = "Pause") : (self.randomButtom.title = "Start")
            })
            .disposed(by: disposeBag)
        
        randomButtom.rx.tap
        .asObservable()
            .subscribe({_ in
            
                self.randomButtom.title == "Start" ? ( self.mainViewModel.isInRandomTime.value = true ):( self.mainViewModel.isInRandomTime.value = false)
        
        })
        .disposed(by: disposeBag)
        
    }

    deinit {
          print("MainViewController deinited correctly")
    }
    
}

