//
//  FavoriteModel.swift
//  Hexad ChallangeCode
//
//  Created by Iman Kazemini on 1/14/20.
//  Copyright © 2020 HexadChallange. All rights reserved.
//

import Foundation
struct FavoriteModel: Codable {
   public var name: String
   public var rate: Double
    
}
