//
//  FavoritesModel.swift
//  Hexad ChallangeCode
//
//  Created by Iman Kazemini on 1/14/20.
//  Copyright © 2020 HexadChallange. All rights reserved.
//

import Foundation


class JosnFavorites{
    
    static func getFavorites() -> String {
        return "[{\"name\":\"Natasha Murashev\",\"rate\":10},{\"name\":\"Jay Freeman\",\"rate\":9},{\"name\":\"Ahmet Yalcinkaya\",\"rate\":8},{\"name\":\"Ayaka Nonaka\",\"rate\":8},{\"name\":\"Matt Galloway\",\"rate\":6},{\"name\":\"Chris Lattner\",\"rate\":9},{\"name\":\"Chris Eidhof\",\"rate\":4},{\"name\":\"Ray Wenderlich\",\"rate\":10},{\"name\":\"Delisa Mason\",\"rate\":6},{\"name\":\"Simon Ng\",\"rate\":7}]"
    }
    
}
