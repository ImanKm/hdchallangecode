//
//  Extensions.swift
//  Hexad ChallangeCode
//
//  Created by Iman Kazemini on 1/14/20.
//  Copyright © 2020 HexadChallange. All rights reserved.
//

import Foundation
extension String {
    func parse<D>(to type: D.Type) -> D? where D: Decodable {

        let data: Data = self.data(using: .utf8)!

        let decoder = JSONDecoder()

        do {
            let _object = try decoder.decode(type, from: data)
            return _object

        } catch {
            return nil
        }
    }
    
    
    
}

