//
//  MainViewControllerUITest.swift
//  Hexad ChallangeCodeUITests
//
//  Created by Iman Kazemini on 1/14/20.
//  Copyright © 2020 HexadChallange. All rights reserved.
//
import XCTest
import Cosmos

class MainViewControllerUITest: XCTestCase {

    override func setUp() {
        continueAfterFailure = false
    }

    override func tearDown() {
       
    }

    func testExample() {
        let app = XCUIApplication()
        app.launch()
    }
    
    func testMainViewTableRows(){
        let tablesQuery = XCUIApplication().tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Natasha Murashev"]/*[[".cells.staticTexts[\"Natasha Murashev\"]",".staticTexts[\"Natasha Murashev\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery.cells.containing(.staticText, identifier:"Natasha Murashev").otherElements["Rating"].tap()
    }
    
    func testMainViewChangeOrderByResort(){
        
        let app = XCUIApplication()
        app.launch()
        
        let table =  app.tables
        let cells = table.cells
        
        var idx = 0
        for cell in cells.allElementsBoundByIndex {
            if !cell.staticTexts["Ahmet Yalcinkaya"].exists {
                idx = idx + 1
            }else{
                break
            }
        }
        let beforeIndex = idx
        
         cells.containing(.staticText, identifier:"Ahmet Yalcinkaya").otherElements["Rating"].tap()
        
        idx = 0
        for cell in cells.allElementsBoundByIndex {
                  if !cell.staticTexts["Ahmet Yalcinkaya"].exists {
                      idx = idx + 1
                  }else{
                      break
                  }
        }
              
        let afterIndex = idx
        
        XCTAssertTrue(beforeIndex != afterIndex)
        
    }
    
     func testMainViewChangeOrderByResortDuringTime(){
        let app = XCUIApplication()
        app.launch()
        
        let makeRandomNavigationBar = XCUIApplication().navigationBars["Make Random"]
            makeRandomNavigationBar.buttons["Start"].tap()
        
       _ = makeRandomNavigationBar.buttons["Pause"].waitForExistence(timeout: 5.1)
         
         makeRandomNavigationBar.buttons["Pause"].tap()
    }
    
    func testLaunchPerformance() {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}
