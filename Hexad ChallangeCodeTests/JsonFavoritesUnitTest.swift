//
//  JsonUnitTest.swift
//  Hexad ChallangeCodeTests
//
//  Created by Iman Kazemini on 1/14/20.
//  Copyright © 2020 HexadChallange. All rights reserved.
//

import Foundation
import XCTest
@testable import Hexad_ChallangeCode

class JsonFavoritesUnitTest: XCTestCase {
    
    func testArraySize() {
        
      let mainViewModel = MainViewModel()
        XCTAssertTrue(mainViewModel.favoritesArray.value.count>=10)
    }
    
    func testArrayStructure() {
        let mainViewModel = MainViewModel()
        for favorit in mainViewModel.favoritesArray.value {
            XCTAssertNotNil(favorit.name)
            XCTAssertNotNil(favorit.rate)
        }
    }
    
    
    
    func testSorting() {
        let mainViewModel = MainViewModel()
            mainViewModel.setSort()
        let favoritesSorted = mainViewModel.favoritesArray.value
        for (index,favorit)  in favoritesSorted.enumerated() {
            if index != 0 && index != favoritesSorted.count-1{
                XCTAssertTrue(favorit.rate <= favoritesSorted[index-1].rate)
            }
         }
    }
    
    
    func testRandomSorting() {
        
        let mainViewModel = MainViewModel()
        mainViewModel.setSort()
        let favoritesSorted = mainViewModel.favoritesArray.value
        
        mainViewModel.setRandom()
        let randomizedFavoritModels = mainViewModel.favoritesArray.value
        
        var flag = false
        for (index,favorit)  in favoritesSorted.enumerated() {
            if favorit.rate != randomizedFavoritModels[index].rate {
                flag = true
            }
         }
        XCTAssertTrue(flag)
    }
    
}
